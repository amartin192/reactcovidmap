// import React, { useState, useEffect } from "react";
import { ComposableMap, Geographies, Geography, Marker } from "react-simple-maps";
import { scaleQuantile, scaleLinear } from "d3-scale";
import { csv } from "d3-fetch";
import React, { Component } from 'react';
import CovidApi from '../Api/CovidLambdaApi'

import { DataTableFromLambda } from '../AwsLambdaParse';

// const geoCountiesUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/counties-10m.json";
const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";

class Map extends Component{
  state = {
    data : null, 
    selectedField: 'active',
    max: 0,
    linearScale: null
  }

  componentDidMount(){
    // csv("./csvTest.csv").then(counties => {
    //   console.log(counties);
    //   this.setState({ data: counties });
    // });
    let { selectedField } = this.state;
    CovidApi.GetAllUSData()
      .then((results) => {
        let data =  DataTableFromLambda(results) 
        let max = data.rows.reduce((calcMax, curr) => {

          if(curr && curr[selectedField]){
            calcMax = Math.max(calcMax, parseInt(curr[selectedField]));
          }

          return calcMax;
        }, 0)
        this.setState({ data, max, linearScale: scaleLinear().domain([1,max]).range([1,20]) });
        
      })
    }

  render(){

    const { data, linearScale, selectedField } = this.state;
    return (
      <ComposableMap projection="geoAlbersUsa">
        <Geographies geography={geoUrl}>
          {({ geographies }) =>
            geographies.map(geo => {
              // const cur = data.find(s => s.id === geo.id);
              return (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  stroke={'#aaa'}
                  fill={"#ddd"}
                />
              );
            })
          }
          
        </Geographies>
        
        <>
          {
            data  && data.rows.map((marker, idx) => {
             return (marker[selectedField] && <Marker key={idx} coordinates={[parseFloat(marker.long), parseFloat(marker.lat)]}>
                <circle r={linearScale(parseInt(marker[selectedField]))}  stroke="#690500"fill="#f26a6399" />

              </Marker>)
            }
            
            )
          }
        </>
        {/* <Marker coordinates={[-74.006, 40.7128]}>
          <circle r={8} stroke="#690500"fill="#f26a6399" />
        </Marker> */}
         
        }
  
          
      </ComposableMap>
    );
  };
  
}

export default Map;
