
import * as config from '../config/default.json'
import fetch from 'node-fetch';
import Promise from 'bluebird';
fetch.Promise = Promise;

const covidApiUrl = config.covidApiUrl;
const covidApiCalls = config.covidApiCalls;

export default {
  GetAllUSData : () => {
    return fetch(`${covidApiUrl}${covidApiCalls.GetAllUSData}`)
      .then(response => response.json())
  }
}