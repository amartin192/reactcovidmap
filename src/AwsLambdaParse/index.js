import testData from './test.json';

const DataTableFromLambda = (data = testData) => {
  
  const { ResultSet: { Rows, ResultSetMetadata } } = data;

  let fields = ResultSetMetadata.ColumnInfo.reduce((newObject, {Name}, index) => {
    newObject[index] = Name.replace("_","");//index;
    return newObject;
  },{})

  let rows = Rows.map(({Data}) => {
    let length = Object.keys(Data).length;
    let results = {};
    for(let i = 0; i < length; i++){
      
      results[fields[i]] = Data[i].VarCharValue;
    }
    return results;
  })

  rows = rows.splice(1)
  rows = rows.filter(row => row.long != null && row.lat != null);

  
  return { columns: fields, rows};
}


export {
  DataTableFromLambda
}